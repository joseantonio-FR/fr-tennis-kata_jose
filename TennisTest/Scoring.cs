namespace TennisTest
{
    public class Scoring
    {
        private readonly string[] _scores = { "love", "fifteen", "thirty", "forty" };
        private Player _player1;
        private Player _player2;

        public Scoring(Player player1, Player player2)
        {
            _player1 = player1;
            _player2 = player2;
        }

        public bool IsGameWinned()
        {
            if ((_player1.Points + 2 == _player2.Points) && (_player2.Points > 3)) return true;
            if ((_player2.Points + 2 == _player1.Points) && (_player1.Points > 3)) return true;
            return false;
        }

        public bool IsDeuce()
        {
            return (_player1.Points == _player2.Points);
        }

        public bool IsPairPoints()
        {
            if (_player1.Points > 3) return false;
            if (_player2.Points > 3) return false;
            return true;
        }

        public string DeuceText()
        {
            return "deuce";
        }

        public string WinnerText()
        {
            return ScoringWinningText("win ");
        }

        public string ScoreText()
        {
            return _scores[_player1.Points] + "-" + _scores[_player2.Points];
        }

        public string AdvantageText()
        {
            return ScoringWinningText("advantage ");
        }

        private string ScoringWinningText(string advantageText)
        {
            if (_player1.Points > _player2.Points) return advantageText + _player1.Name;
            return advantageText + _player2.Name;
        }
    }
}