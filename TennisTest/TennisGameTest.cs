﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tennis_kata;

namespace TennisTest
{
    [TestClass]
    public class TennisGameTest
    {
        private TennisGame _game;

        [TestInitialize]
        public void Ini()
        {
            _game= new TennisGame("Sampras","Federer");
        }

        [TestMethod]
        public void Scoring_ShouldReturnLoveLove_WhenGameStart()
        {
            Assert.AreEqual("love-love", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnFifteenLove_WhenFirstPlayerAnnotate()
        {
            _game.AnnotateFirstPlayer();
            Assert.AreEqual("fifteen-love", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnLoveFifteen_WhenSecondPlayerAnnotate()
        {
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("love-fifteen", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnFifteenFifteen_WhenBothPlayersAnnotateOnePointAnnotingFirstTheFirstPlayer()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("fifteen-fifteen", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnFifteenFifteen_WhenBothPlayersAnnotateOnePointAnnotingFirstTheSecondPlayer()
        {
            _game.AnnotateSecondPlayer();
            _game.AnnotateFirstPlayer();
            Assert.AreEqual("fifteen-fifteen", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnThirtyLove_WhenFirstPlayerAnnotate2Points()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            Assert.AreEqual("thirty-love", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnLoveThirty_WhenSecondPlayerAnnotate2Points()
        {
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("love-thirty", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnThirtyFifteen_WhenFirstPlayerAnnotate2PointsAnsSecondOnly1()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("thirty-fifteen", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnFifteenThirty_WhenSecondPlayerAnnotate2PointsAndFirstOnly1()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("fifteen-thirty", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnFortyLove_WhenFirstPlayerAnnotate3Points()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            Assert.AreEqual("forty-love", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnLoveForty_WhenSecondPlayerAnnotate3Points()
        {
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("love-forty", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnFortyFifteen_WhenFirstPlayerAnnotate3PointsAnsSecondOnly1()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("forty-fifteen", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnFifteenForty_WhenSecondPlayerAnnotate3PointsAndFirstOnly1()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("fifteen-forty", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnFortyThirty_WhenFirstPlayerAnnotate3PointsAnsSecondOnly2()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("forty-thirty", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnThirtyForty_WhenSecondPlayerAnnotate3PointsAndFirstOnly2()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("thirty-forty", _game.Scoring());
        }


        [TestMethod]
        public void Scoring_ShouldReturnFortyForty_WhenBothPlayersAnnotate3Points()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("forty-forty", _game.Scoring());
        }


        [TestMethod]
        public void Scoring_ShouldReturnAdvantageFirstPlayer_WhenFirstPlayerAnnotate4PointsAndSecondPlayer3Points()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateFirstPlayer();
            Assert.AreEqual("advantage Sampras", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnAdvantageSecondPlayer_WhenFirstPlayerAnnotate3PointsAndSecondPlayer4Points()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("advantage Federer", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnAdvantagesAndDeuce_WhenAdvantageChanges()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateFirstPlayer();
            Assert.AreEqual("deuce", _game.Scoring());
            _game.AnnotateFirstPlayer();
            Assert.AreEqual("advantage Sampras", _game.Scoring());
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("deuce", _game.Scoring());
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("advantage Federer", _game.Scoring());
            _game.AnnotateFirstPlayer();
            Assert.AreEqual("deuce", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnWinFirstPlayer_WhenPlayerOneHas4PointsAndPlayerTwoHas2()
        {
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            Assert.AreEqual("win Sampras", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnWinSecondPlayer_WhenPlayerOneHas2PointsAndPlayerTwoHas4()
        {
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            Assert.AreEqual("win Federer", _game.Scoring());
        }

        [TestMethod]
        public void Scoring_ShouldReturnWinFirstPlayer_WhenPlayerOneWinAfterAdvantage()
        {
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateSecondPlayer();
            _game.AnnotateFirstPlayer();
            _game.AnnotateFirstPlayer();
            Assert.AreEqual("advantage Sampras", _game.Scoring());
            _game.AnnotateFirstPlayer();
            Assert.AreEqual("win Sampras", _game.Scoring());
        }

    }
}
