namespace TennisTest
{
    public class TennisGame
    {
        private readonly Player _player1;
        private readonly Player _player2;

        public TennisGame(string player1Name, string player2Name)
        {
            _player1 = new Player { Name = player1Name, Points = 0 };
            _player2 = new Player { Name = player2Name, Points = 0 };
        }

        public string Scoring()
        {
            var scoring = new Scoring(_player1, _player2);
            if (scoring.IsPairPoints()) return scoring.ScoreText();
            if (scoring.IsGameWinned()) return scoring.WinnerText();
            if (scoring.IsDeuce()) return scoring.DeuceText();
            return scoring.AdvantageText();
        }

        public void AnnotateFirstPlayer()
        {
            _player1.Points++;
        }

        public void AnnotateSecondPlayer()
        {
            _player2.Points++;
        }
    }
}